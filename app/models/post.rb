class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy #add association to comments
	validates_presence_of :title #add validators
	validates_presence_of :body

end
